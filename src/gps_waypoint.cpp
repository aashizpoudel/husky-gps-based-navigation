#include <actionlib/client/simple_action_client.h>
#include <actionlib/server/simple_action_server.h>
#include <fstream>
#include <geometry_msgs/PointStamped.h>
#include <gps_based_navigation/GoToWaypointAction.h>
#include <gps_based_navigation/GoToWaypointFeedback.h>
#include <gps_based_navigation/GoToWaypointGoal.h>
#include <gps_based_navigation/GoToWaypointResult.h>
#include <math.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <robot_localization/navsat_conversions.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <tf/transform_listener.h>
#include <utility>
#include <vector>

#include <sensor_msgs/NavSatFix.h>

// initialize variables

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction>
    MoveBaseClient; // create a type definition for a client called
// MoveBaseClient
geometry_msgs::PointStamped UTM_point, map_point, UTM_next, map_next;

std::string utm_zone;

geometry_msgs::PointStamped latLongtoUTM(double lati_input,
                                         double longi_input) {
  double utm_x = 0, utm_y = 0;
  geometry_msgs::PointStamped UTM_point_output;

  // convert lat/long to utm
  RobotLocalization::NavsatConversions::LLtoUTM(lati_input, longi_input, utm_y,
                                                utm_x, utm_zone);

  // Construct UTM_point and map_point geometry messages
  UTM_point_output.header.frame_id = "utm";
  UTM_point_output.header.stamp = ros::Time(0);
  UTM_point_output.point.x = utm_x;
  UTM_point_output.point.y = utm_y;
  UTM_point_output.point.z = 0;

  return UTM_point_output;
}

geometry_msgs::PointStamped
UTMtoMapPoint(geometry_msgs::PointStamped UTM_input) {
  geometry_msgs::PointStamped map_point_output;
  bool notDone = true;
  tf::TransformListener listener;
  ros::Time time_now = ros::Time::now();
  while (notDone) {
    try {
      UTM_point.header.stamp = ros::Time::now();
      listener.waitForTransform("odom", "utm", time_now, ros::Duration(3.0));
      listener.transformPoint("odom", UTM_input, map_point_output);
      notDone = false;
    } catch (tf::TransformException &ex) {
      ROS_WARN("%s", ex.what());
      ros::Duration(0.01).sleep();
      // return;
    }
  }
  return map_point_output;
}

move_base_msgs::MoveBaseGoal buildGoal(geometry_msgs::PointStamped map_point,
                                       geometry_msgs::PointStamped map_next,
                                       bool last_point) {
  move_base_msgs::MoveBaseGoal goal;

  // Specify what frame we want the goal to be published in
  goal.target_pose.header.frame_id = "odom";
  goal.target_pose.header.stamp = ros::Time::now();

  // Specify x and y goal
  goal.target_pose.pose.position.x = map_point.point.x; // specify x goal
  goal.target_pose.pose.position.y = map_point.point.y; // specify y goal

  if (last_point == false) {
    tf::Matrix3x3 rot_euler;
    tf::Quaternion rot_quat;

    // Calculate quaternion
    float x_curr = map_point.point.x, y_curr = map_point.point.y;
    float x_next = map_next.point.x, y_next = map_next.point.y;
    float delta_x = x_next - x_curr, delta_y = y_next - y_curr;
    float yaw_curr = 0, pitch_curr = 0, roll_curr = 0;
    yaw_curr = atan2(delta_y, delta_x);

    // Specify quaternions
    rot_euler.setEulerYPR(yaw_curr, pitch_curr, roll_curr);
    rot_euler.getRotation(rot_quat);

    goal.target_pose.pose.orientation.x = rot_quat.getX();
    goal.target_pose.pose.orientation.y = rot_quat.getY();
    goal.target_pose.pose.orientation.z = rot_quat.getZ();
    goal.target_pose.pose.orientation.w = rot_quat.getW();
  } else {
    goal.target_pose.pose.orientation.w = 1.0;
  }

  return goal;
}

class GoToWaypointAction {
protected:
  ros::NodeHandle nh_;
  actionlib::SimpleActionServer<gps_based_navigation::GoToWaypointAction>
      as_; // NodeHandle instance must be created before this line. Otherwise
           // strange error occurs.
  std::string action_name_;
  // create messages that are used to published feedback/result
  gps_based_navigation::GoToWaypointFeedback feedback_;
  gps_based_navigation::GoToWaypointResult result_;
  std::vector<std::pair<double, double>> waypointVect;
  std::vector<std::pair<double, double>>::iterator iter; // init. iterator
  geometry_msgs::PointStamped UTM_point, map_point, UTM_next, map_next;
  int count = 0, waypointCount = 0, wait_count = 0;
  double numWaypoints = 0;
  double latiGoal, longiGoal, latiNext, longiNext;
  std::string utm_zone;
  std::string path_local, path_abs;
  MoveBaseClient ac;

public:
  GoToWaypointAction(std::string name)
      : as_(nh_, name, boost::bind(&GoToWaypointAction::executeCB, this, _1),
            false),
        action_name_(name), ac("/move_base", true) {
    as_.start();
    while (!ac.waitForServer(ros::Duration(5.0))) {
      wait_count++;
      if (wait_count > 3) {
        ROS_ERROR("move_base action server did not come up, killing "
                  "gps_waypoint node...");
        // Notify joy_launch_control that waypoint following is complete
        std_msgs::Bool node_ended;
        node_ended.data = true;
        // pubWaypointNodeEnded.publish(node_ended);
        ros::shutdown();
      }
      ROS_INFO("Waiting for the move_base action server to come up");
    }
  }

  ~GoToWaypointAction(void) {}

  void executeCB(const gps_based_navigation::GoToWaypointGoalConstPtr &goal) {
    // helper variables
    ros::Rate r(1);
    bool success = true;
    bool final_point = false;
    for (int i = 0; i < goal->points.size(); ++i) {
      const gps_based_navigation::GPSGoal &data = goal->points[i];
      latiGoal = data.lat;
      longiGoal = data.lng;
      bool final_point = false;

      // set next goal point if not at last waypoint
      if (i < goal->points.size() - 1) {
        // iter++;
        latiNext = goal->points[i + 1].lat;
        longiNext = goal->points[i + 1].lng;
      } else {
        latiNext = goal->points[i].lat;
        longiNext = goal->points[i].lng;
        final_point = true;
      }

      ROS_INFO("Received Latitude goal:%.8f", latiGoal);
      ROS_INFO("Received longitude goal:%.8f", longiGoal);

      UTM_point = latLongtoUTM(latiGoal, longiGoal);
      UTM_next = latLongtoUTM(latiNext, longiNext);

      // Transform UTM to map point in odom frame
      map_point = UTMtoMapPoint(UTM_point);
      map_next = UTMtoMapPoint(UTM_next);

      // Build goal to send to move_base
      move_base_msgs::MoveBaseGoal mgoal =
          buildGoal(map_point, map_next, final_point);

      // Send Goal
      ROS_INFO("Sending goal");
      ac.sendGoal(mgoal); // push goal to move_base node

      ac.waitForResult();

      if (ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
        ROS_INFO("Husky has reached its goal!");
      } else {
        ROS_ERROR(
            "Husky was unable to reach its goal. GPS Waypoint unreachable.");
        // ROS_INFO("Exiting node...");
        // Notify joy_launch_control that waypoint following iscomplete
        success = false;
      }
    }

    result_.result = success;
    if (success) {

      ROS_INFO("%s: Succeeded", action_name_.c_str());
    }
    as_.setSucceeded(result_);
  }
};

int main(int argc, char **argv) {
  ros::init(argc, argv, "gps_waypoint");

  GoToWaypointAction fibonacci("gps_waypoint");
  ros::spin();

  return 0;
}
