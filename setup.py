from distutils.core import setup

setup(
    version='1.0.0',

    scripts=['scripts/gps_based_navigation/gps_based_navigation.py'],

    packages=['gps_based_navigation'],

    package_dir={'':'scripts'}
)