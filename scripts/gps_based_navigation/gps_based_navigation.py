#!/usr/bin/python
import rospy
import click
import math
import actionlib
import tf2_ros

from geographiclib.geodesic import Geodesic
from actionlib_msgs.msg import GoalStatus
from geometry_msgs.msg import PoseStamped
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from sensor_msgs.msg import NavSatFix
import geometry_msgs.msg as gmsg
import tkinter as tk
import sensor_msgs.msg as smsg
import utm
import tf
import tf2_geometry_msgs

def DMS_to_decimal_format(lat,long):
  # Check for degrees, minutes, seconds format and convert to decimal
  if ',' in lat:
    degrees, minutes, seconds = lat.split(',')
    degrees, minutes, seconds = float(degrees), float(minutes), float(seconds)
    if lat[0] == '-': # check for negative sign
      minutes = -minutes
      seconds = -seconds
    lat = degrees + minutes/60 + seconds/3600
  if ',' in long:
    degrees, minutes, seconds = long.split(',')
    degrees, minutes, seconds = float(degrees), float(minutes), float(seconds)
    if long[0] == '-': # check for negative sign
      minutes = -minutes
      seconds = -seconds
    long = degrees + minutes/60 + seconds/3600

  lat = float(lat)
  long = float(long)
  rospy.loginfo('Given GPS goal: lat %s, long %s.' % (lat, long))
  return lat, long

def get_origin_lat_long():
  # Get the lat long coordinates of our map frame's origin which must be publshed on topic /local_xy_origin. We use this to calculate our goal within the map frame.
  rospy.loginfo("Waiting for a message to initialize the origin GPS location...")
  origin_pose = rospy.wait_for_message('local_xy_origin', PoseStamped)
  origin_lat = origin_pose.pose.position.y
  origin_long = origin_pose.pose.position.x
  easting,northing,_,_ = utm.from_latlon(origin_lat, origin_long)
  rospy.loginfo('Received origin: lat %s, long %s, east %s, norhing %s' % (origin_lat, origin_long,easting,northing))
  return origin_lat, origin_long

def calc_goal(origin_lat, origin_long, goal_lat, goal_long):
  # Calculate distance and azimuth between GPS points
  to_utm = utm.from_latlon(goal_lat, goal_long)
  geod = Geodesic.WGS84  # define the WGS84 ellipsoid
  g = geod.Inverse(origin_lat, origin_long, goal_lat, goal_long) # Compute several geodesic calculations between two GPS points 
  rospy.loginfo(g)
  hypotenuse = distance = g['s12'] # access distance
  rospy.loginfo("The distance from the origin to the goal is {:.3f} m.".format(distance))
  azimuth = g['azi1']
  rospy.loginfo("The azimuth from the origin to the goal is {:.3f} degrees.".format(azimuth))

  # Convert polar (distance and azimuth) to x,y translation in meters (needed for ROS) by finding side lenghs of a right-angle triangle
  # Convert azimuth to radians
  azimuth = math.radians(360-azimuth)
  x = adjacent = math.cos(azimuth) * hypotenuse
  y = opposite = math.sin(azimuth) * hypotenuse
  rospy.loginfo("The translation from the origin to the goal is (x,y) {:.3f}, {:.3f} m.".format(x, y))

  return x, y

class GpsGoal():

  def joy_msg_callback(msg):
    self.collect = message.buttons[0] == 1

  def scb_callback(self):
    self.start_working = not self.start_working 
    self.start_collecting_btn_text.set("Stop collecting")
  
  def swb_callback(self):
    self.start_working = not self.start_working 
    self.start_working_btn_text.set("Stop working")

 
  def gps_to_goal(self,latitude,longitude):
    # easting,northing,_,_ = utm.from_latlon(latitude, longitude)
    easting,northing = calc_goal(self.origin_lat, self.origin_long, latitude, longitude)
    rospy.loginfo(str(easting)+" "+str(northing))
    try:
        point_base = tf2_geometry_msgs.PointStamped()
        # point_base.header.frame_id = 'utm'
        point_base.header.frame_id = 'map'
        point_base.header.stamp = rospy.get_rostime()
        point_base.point.x = easting  
        point_base.point.y = northing
        point_base.point.z = 0

        # point_odom = self.tf_buffer.transform(point_base, 'map',
        #                                       rospy.Duration(1.0))
        # rospy.sleep(.5)
        point_odom = point_base

        return point_odom

    except tf2_ros.TransformException as e:
        print(type(e))
        print("(May not be a big deal.)")
    return False
  def sendTransform(self,x,y,z=0):
    broadcaster = tf2_ros.TransformBroadcaster()
    static_transformStamped = gmsg.TransformStamped()
    static_transformStamped.header.stamp = rospy.Time.now()
    static_transformStamped.header.frame_id = "goal"
    static_transformStamped.child_frame_id = "goal"
    static_transformStamped.transform.translation.x = x
    static_transformStamped.transform.translation.y = y
    static_transformStamped.transform.translation.z = z
    static_transformStamped.transform.rotation.x = 0
    static_transformStamped.transform.rotation.y = 0
    static_transformStamped.transform.rotation.z = 0
    static_transformStamped.transform.rotation.w = 1
    broadcaster.sendTransform(static_transformStamped)


  def __init__(self):
    super(GpsGoal, self).__init__()
    rospy.init_node('gps_goal')
    self.goals = []
    rospy.loginfo("Connecting to move_base...")
    self.move_base = actionlib.SimpleActionClient('move_base', MoveBaseAction)
    self.move_base.wait_for_server()
    rospy.loginfo("Connected.")
    self.tf_buffer = tf2_ros.Buffer()
    tf2_ros.TransformListener(self.tf_buffer)
    # self.tf_buffe
  

    rospy.Subscriber('gps_goal_pose', PoseStamped, self.gps_goal_pose_callback)
    rospy.Subscriber('gps_goal_fix', NavSatFix, self.gps_goal_fix_callback)

    # Get the lat long coordinates of our map frame's origin which must be publshed on topic /local_xy_origin. We use this to calculate our goal within the map frame.
    self.origin_lat, self.origin_long = get_origin_lat_long()

    self.origin_easting,self.origin_northing,_ ,_  = utm.from_latlon(self.origin_lat, self.origin_long)

  def do_gps_goal(self, goal_lat, goal_long, z=0, yaw=0, roll=0, pitch=0):
    # Calculate goal x and y in the frame_id given the frame's origin GPS and a goal GPS location
    output = self.gps_to_goal(goal_lat, goal_long)
    # Create move_base goal
    if output:
      print("to map point",output)
      self.publish_goal(x=output.point.x, y=output.point.y, z=z, yaw=yaw, roll=roll, pitch=pitch)

  def gps_goal_pose_callback(self, data):
    lat = data.pose.position.y
    long = data.pose.position.x
    z = data.pose.position.z
    euler = tf.transformations.euler_from_quaternion(data.pose.orientation)
    roll = euler[0]
    pitch = euler[1]
    yaw = euler[2]
    self.do_gps_goal(lat, long, z=z, yaw=yaw, roll=roll, pitch=pitch)

  def gps_goal_fix_callback(self, data):
    rospy.loginfo("Received:"+str(data.latitude) + " " + str(data.longitude))
    self.do_gps_goal(data.latitude, data.longitude)

  def publish_goal(self, x=0, y=0, z=0, yaw=0, roll=0, pitch=0):
    # Create move_base goal
    goal = MoveBaseGoal()
    goal.target_pose.header.frame_id = "map"
    goal.target_pose.pose.position.x = x
    goal.target_pose.pose.position.y = y
    goal.target_pose.pose.position.z =  z
    quaternion = tf.transformations.quaternion_from_euler(roll, pitch, yaw)
    goal.target_pose.pose.orientation.x = quaternion[0]
    goal.target_pose.pose.orientation.y = quaternion[1]
    goal.target_pose.pose.orientation.z = quaternion[2]
    goal.target_pose.pose.orientation.w = quaternion[3]
    rospy.loginfo('Executing move_base goal to position (x,y) %s, %s, with %s degrees yaw.' %
            (x, y, yaw))
    rospy.loginfo("To cancel the goal: 'rostopic pub -1 /move_base/cancel actionlib_msgs/GoalID -- {}'")

    # Send goal
    self.move_base.send_goal(goal)
    rospy.loginfo('Inital goal status: %s' % GoalStatus.to_string(self.move_base.get_state()))
    status = self.move_base.get_goal_status_text()
    if status:
      rospy.loginfo(status)

    # Wait for goal result
    self.move_base.wait_for_result()
    rospy.loginfo('Final goal status: %s' % GoalStatus.to_string(self.move_base.get_state()))
    status = self.move_base.get_goal_status_text()
    if status:
      rospy.loginfo(status)


def ros_main():
  gpsGoal = GpsGoal()
  rospy.spin()

if __name__ == '__main__':
  ros_main()


